import java.util.List;

public class ThoiKhoaBieu {
    private String IdSubject;
    private String NameSubject;
    private String ClassRoom;
    private String IdClass;

    public ThoiKhoaBieu() {
        IdSubject = "";
        NameSubject = "";
        ClassRoom = "";
        IdClass = "";
    }

    public ThoiKhoaBieu(String idSubject, String nameSubject, String classRoom, String idClass) {
        this.IdSubject = idSubject;
        this.NameSubject = nameSubject;
        this.ClassRoom = classRoom;
        this.IdClass = idClass;
    }

    public String getIdSubject() {
        return IdSubject;
    }

    public void setIdSubject(String idSubject) {
        this.IdSubject = idSubject;
    }

    public String getNameSubject() {
        return NameSubject;
    }

    public void setNameSubject(String nameSubject) {
        this.NameSubject = nameSubject;
    }

    public String getClassRoom() {
        return ClassRoom;
    }

    public void setClassRoom(String classRoom) {
        this.ClassRoom = classRoom;
    }

    public String getIdClass() {
        return IdClass;
    }

    public void setIdClass(String idClass) {
        this.IdClass = idClass;
    }


    @Override
    public String toString() {
        return "ThoiKhoaBieu{" +
                "IdSubject='" + IdSubject + '\'' +
                ", NameSubject='" + NameSubject + '\'' +
                ", ClassRoom='" + ClassRoom + '\'' +
                ", IdClass='" + IdClass + '\'' +
                '}';
    }
}
