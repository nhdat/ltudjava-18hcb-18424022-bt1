import java.util.List;

public class SinhVien {
    private String MSSV;
    private String Name;
    private String Gender;
    private String IdCard;
    private String IdClass;

    public SinhVien(String MSSV, String name, String gender, String idCard, String idClass) {
        this.MSSV = MSSV;
        this.Name = name;
        this.Gender = gender;
        this.IdCard = idCard;
        this.IdClass = idClass;
    }

    public String getIdClass() {
        return IdClass;
    }

    public void setIdClass(String idClass) {
        IdClass = idClass;
    }

    public String getMSSV() {
        return MSSV;
    }

    public void setMSSV(String MSSV) {
        this.MSSV = MSSV;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getIdCard() {
        return IdCard;
    }

    public void setIdCard(String idCard) {
        IdCard = idCard;
    }

    @Override
    public String toString() {
        return "SinhVien{" +
                "MSSV='" + MSSV + '\'' +
                ", Name='" + Name + '\'' +
                ", Gender='" + Gender + '\'' +
                ", IdCard='" + IdCard + '\'' +
                ", IdClass='" + IdClass + '\'' +
                '}';
    }
}
