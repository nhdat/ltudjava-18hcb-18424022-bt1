public class Account {
    private String User;
    private String Pwd;
    private String Role;

    public Account(String user, String pwd, String role) {
        User = user;
        Pwd = pwd;
        Role = role;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public String getPwd() {
        return Pwd;
    }

    public void setPwd(String pwd) {
        Pwd = pwd;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }
}
