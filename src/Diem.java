public class Diem {
    private String MSSV;
    private String Name;
    private Float DiemGK;
    private Float DiemCK;
    private Float DiemKhac;

    public Diem(String MSSV, String name, Float diemGK, Float diemCK, Float diemKhac, Float diemTong) {
        this.MSSV = MSSV;
        Name = name;
        DiemGK = diemGK;
        DiemCK = diemCK;
        DiemKhac = diemKhac;
        DiemTong = diemTong;
    }

    private Float DiemTong;

    public String getMSSV() {
        return MSSV;
    }

    public void setMSSV(String MSSV) {
        this.MSSV = MSSV;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Float getDiemGK() {
        return DiemGK;
    }

    public void setDiemGK(Float diemGK) {
        DiemGK = diemGK;
    }

    public Float getDiemCK() {
        return DiemCK;
    }

    public void setDiemCK(Float diemCK) {
        DiemCK = diemCK;
    }

    public Float getDiemKhac() {
        return DiemKhac;
    }

    public void setDiemKhac(Float diemKhac) {
        DiemKhac = diemKhac;
    }

    public Float getDiemTong() {
        return DiemTong;
    }

    public void setDiemTong(Float diemTong) {
        DiemTong = diemTong;
    }

    @Override
    public String toString() {
        return "Diem{" +
                "MSSV='" + MSSV + '\'' +
                ", Name='" + Name + '\'' +
                ", DiemGK=" + DiemGK +
                ", DiemCK=" + DiemCK +
                ", DiemKhac=" + DiemKhac +
                ", DiemTong=" + DiemTong +
                '}';
    }
}
