import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws IOException {
        Login();
    }

    private static void Login() throws IOException {
        System.out.print("Nhập tài khoản: ");
        Scanner scanner = new Scanner(System.in);
        String user = scanner.nextLine();

        System.out.print("Nhập mật khẩu: ");
        String pwd = scanner.nextLine();

        String pathName = "./properties/files/Account.csv";

        List<Account> lstAccount= new ArrayList<>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathName));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("user")) {
                Account accountTmp = new Account("", "", "");

                accountTmp.setUser(data[0]);
                accountTmp.setPwd(data[1]);
                accountTmp.setRole(data[2]);

                lstAccount.add(accountTmp);
            }
        }
        csvReader.close();

        for(Account acc : lstAccount){
            if(acc.getUser().equals(user) && acc.getPwd().equals(pwd) && acc.getRole().equals("gv")){
                showMenu();
                break;
            }else if(acc.getUser().equals(user) && acc.getPwd().equals(pwd) && acc.getRole().equals("sv")){
                showMenuSinhVien(acc.getUser());
                break;
            }
        }
    }

    private static void showMenuRequest1() throws IOException {
        int choose = 0;
        do {
            System.out.println("----------- MENU 1-----------");
            System.out.println("0. Exit");
            System.out.println("1. Xem danh sách sinh viên 1 lớp học.");
            System.out.println("2. Xem danh sách sinh viên theo lớp học và môn học.");
            System.out.print("\n=> Mời bạn nhập lựa chọn: ");
            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();

            if (isInteger(line)) {
                choose = Integer.parseInt(line);
                if(choose == 1){
                    System.out.println("Nhập mã lớp: ");
                    String idClass = scanner.nextLine();

                    String pathSystem = "./properties/files/System.csv";
                    List<SinhVien> lstSv = readFileSinhVienByIdClass(pathSystem, idClass);

                    List<Object> lstObj = new ArrayList<Object>();
                    lstObj.addAll(lstSv);

                    printArray(lstObj);
                }else if(choose == 2){
                    System.out.println("Nhập mã lớp - môn học: ");
                    String idClassSubject = scanner.nextLine();

                    String pathSystem = "./properties/files/" + idClassSubject + ".csv";
                    List<SinhVien> lstSv = readFileSinhVienTKB(pathSystem);

                    List<Object> lstObj = new ArrayList<Object>();
                    lstObj.addAll(lstSv);

                    printArray(lstObj);
                }else if(choose == 0){
                    break;
                }
            }
        } while (true);
    }

    private static void showMenu() throws IOException {
        int choose = 0;
        do {
            System.out.println("----------- MENU -----------");
            System.out.println("0. Exit");
            System.out.println("1. Xem danh sách sinh viên.");
            System.out.println("2. Import danh sach sinh viên.");
            System.out.println("3. Thêm sinh viên vào hệ thống.");
            System.out.println("4. Import thời khóa biểu.");
            System.out.println("5. Xem danh sách thời khóa biểu.");
            System.out.println("6. Sinh viên hủy đăng ký môn.");
            System.out.println("7. Sinh viên đăng ký môn.");
            System.out.println("8. Import bảng điểm.");
            System.out.println("9. Xem bảng điểm / Thống kê");
            System.out.println("10. Sửa điểm.");
            System.out.println("11. Đổi mật khẩu.");

            System.out.print("\n=> Mời bạn nhập lựa chọn: ");
            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();

            if (isInteger(line)) {
                choose = Integer.parseInt(line);
                if(choose == 1){
                    showMenuRequest1();
                }else if (choose == 2) {
                    String pathToImport = "./properties/files/import/SinhVien/";
                    List<String> lstFile = listFilesForFolder(pathToImport);
                    for (String file : lstFile) {
                        String pathFile = pathToImport + file;

                        String[] arr = file.split("\\.");
                        List lstSV = readFileSinhVien(pathFile, arr[0]);
                        String pathFileSystem = "./properties/files/System.csv";
                        writeFileSystem(lstSV, pathFileSystem);

                        System.out.println("Import thành công danh sách lớp " + arr[0]);
                    }
                }else if(choose == 3){
                    SinhVien svInput = new SinhVien("", "", "", "", "");
                    Scanner svScanner = new Scanner(System.in);
                    System.out.println("Nhập MSSV: ");
                    svInput.setMSSV(svScanner.nextLine());
                    System.out.println("Nhập họ tên: ");
                    svInput.setName(svScanner.nextLine());
                    System.out.println("Nhập CMND: ");
                    svInput.setIdCard(svScanner.nextLine());
                    System.out.println("Nhập mã lớp: ");
                    svInput.setIdClass(svScanner.nextLine());

                    List<SinhVien> lstSV = new ArrayList<SinhVien>();
                    lstSV.add(svInput);
                    String pathFileSystem = "./properties/files/System.csv";
                    writeFileSystem(lstSV, pathFileSystem);

                    System.out.println("Thêm thành công sinh viên " + svInput.getMSSV());

                }else if(choose == 4){
                    String pathToImport = "./properties/files/import/GiaoVu/";
                    List<String> lstFile = listFilesForFolder(pathToImport);
                    for (String file : lstFile) {
                        String pathFile = pathToImport + file;

                        String[] arr = file.split("\\.");
                        List lstTKB = readFileTKB(pathFile, arr[0]);
                        String pathFileSystem = "./properties/files/SystemTKB.csv";
                        writeFileSystemTKB(lstTKB, pathFileSystem);

                        System.out.println("Import thành công thời khóa biểu lớp " + arr[0]);
                    }
                }else if(choose == 5){
                    System.out.println("Nhập mã lớp: ");
                    String idClass = scanner.nextLine();

                    String pathSystem = "./properties/files/SystemTKB.csv";
                    List<ThoiKhoaBieu> lstTKB = readFileSystemTKBByIdClass(pathSystem, idClass);

                    List<Object> lstObj = new ArrayList<Object>();
                    lstObj.addAll(lstTKB);

                    printArray(lstObj);
                }else if(choose == 6){
                    System.out.print("\n=> Nhập mã số sinh viên: ");
                    Scanner scanner2 = new Scanner(System.in);
                    String mssv = scanner2.nextLine();

                    System.out.print("\n=> Nhập mã lớp học: ");
                    Scanner scanner3 = new Scanner(System.in);
                    String idClass = scanner3.nextLine();

                    System.out.print("\n=> Nhập mã môn: ");
                    Scanner scanner4 = new Scanner(System.in);
                    String idSubject = scanner4.nextLine();

                    String namePath = "./properties/files/" + idClass + "-" + idSubject + ".csv";
                    removeStudentFromSystem(namePath, idClass, mssv);
                }else if(choose == 7){
                    System.out.print("\n=> Nhập mã số sinh viên: ");
                    Scanner scanner2 = new Scanner(System.in);
                    String mssv = scanner2.nextLine();

                    System.out.print("\n=> Nhập mã lớp học: ");
                    Scanner scanner3 = new Scanner(System.in);
                    String idClass = scanner3.nextLine();

                    System.out.print("\n=> Nhập mã môn: ");
                    Scanner scanner4 = new Scanner(System.in);
                    String idSubject = scanner4.nextLine();

                    String namePath = "./properties/files/" + idClass + "-" + idSubject + ".csv";
                    addStudentToSystem(namePath, idClass, idSubject, mssv);
                }else if(choose == 8){
                    String pathToImport = "./properties/files/import/Diem/";
                    List<String> lstFile = listFilesForFolder(pathToImport);
                    for (String file : lstFile) {
                        String pathFile = pathToImport + file;

                        String[] arr = file.split("\\.");
                        List lstDiem = readFileDiem(pathFile, arr[0]);
                        String pathFileSystem = "./properties/files/Diem/" + file;
                        writeFileSystemDiem(lstDiem, pathFileSystem);

                        System.out.println("Import thành công điểm của lớp " + arr[0]);
                    }
                }else if(choose == 9){
                    System.out.print("\n=> Nhập mã số lớp - mã môn: ");
                    Scanner scanner2 = new Scanner(System.in);
                    String idClassSubject = scanner2.nextLine();

                    String pathFile = "./properties/files/Diem/" + idClassSubject + ".csv";
                    analyticsDiem(pathFile);
                }else if(choose == 10){
                    System.out.print("\n=> Nhập mã số lớp - mã môn: ");
                    Scanner scanner2 = new Scanner(System.in);
                    String idClassSubject = scanner2.nextLine();

                    System.out.print("\n=> Nhập mssv: ");
                    String mssv = scanner2.nextLine();

                    System.out.print("\n=> Nhập điểm giữa kỳ: ");
                    float diemGK = scanner2.nextFloat();

                    System.out.print("\n=> Nhập điểm cuối kỳ: ");
                    float diemCK = scanner2.nextFloat();

                    System.out.print("\n=> Nhập điểm khác: ");
                    float diemKhac = scanner2.nextFloat();

                    System.out.print("\n=> Nhập điểm tổng: ");
                    float diemTong = scanner2.nextFloat();

                    editDiemSinhVien(mssv, idClassSubject, diemGK, diemCK, diemKhac, diemTong);
                }else if(choose == 11){
                    changePwd();
                }else if(choose == 0){
                    break;
                }
            }
        } while (true);
    }

    private static void showMenuSinhVien(String mssv) throws IOException{
        int choose = 0;
        do {
            System.out.println("----------- MENU -----------");
            System.out.println("0. Exit");
            System.out.println("1. Xem điểm cá nhân");
            System.out.println("2. Đổi mật khẩu.");

            System.out.print("\n=> Mời bạn nhập lựa chọn: ");
            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();

            if (isInteger(line)) {
                choose = Integer.parseInt(line);
                if(choose == 1){
                    System.out.print("\n=> Nhập mã lớp - môn: ");
                    String idSubject = scanner.nextLine();

                    String path = "./properties/files/Diem/" + idSubject + ".csv";

                    BufferedReader csvReader = new BufferedReader(new FileReader(path));
                    String row;
                    while ((row = csvReader.readLine()) != null) {
                        String[] data = row.split(",");
                        if (!data[0].equals("STT") && data[0].equals(mssv)) {
                            Diem diemTmp = new Diem("", "", (float) 0/1, (float)0/1, (float)0/1, (float)0/1);

                            diemTmp.setMSSV(data[0]);
                            diemTmp.setName(data[1]);
                            diemTmp.setDiemGK(Float.parseFloat(data[2]));
                            diemTmp.setDiemCK(Float.parseFloat(data[3]));
                            diemTmp.setDiemKhac(Float.parseFloat(data[4]));
                            diemTmp.setDiemTong(Float.parseFloat(data[5]));

                            System.out.println(diemTmp.toString());
                        }
                    }
                    csvReader.close();

                }else if (choose == 2) {
                    changePwd();
                }else if(choose == 0){
                    break;
                }
            }
        } while (true);
    }

    private static void changePwd() throws IOException {
        System.out.print("Nhập tài khoản: ");
        Scanner scanner = new Scanner(System.in);
        String user = scanner.nextLine();

        System.out.print("Nhập mật khẩu: ");
        String pwd = scanner.nextLine();

        System.out.print("Nhập mật khẩu mới: ");
        String newPwd = scanner.nextLine();

        String pathName = "./properties/files/Account.csv";

        List<Account> lstAccount= new ArrayList<>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathName));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("user")) {
                Account accountTmp = new Account("", "", "");

                accountTmp.setUser(data[0]);
                accountTmp.setPwd(data[1]);
                accountTmp.setRole(data[2]);

                lstAccount.add(accountTmp);
            }
        }
        csvReader.close();

        for(Account acc : lstAccount){
            if(acc.getUser().equals(user) && acc.getPwd().equals(pwd)){
                acc.setPwd(newPwd);
                break;
            }
        }

        FileWriter csvWriter = new FileWriter(pathName, false);
        csvWriter.append("user,pwd,role\n");

        for (Account acc : lstAccount) {
            csvWriter.append(acc.getUser());
            csvWriter.append(",");
            csvWriter.append(acc.getPwd());
            csvWriter.append(",");
            csvWriter.append(acc.getRole());
            csvWriter.append("\n");
        }

        csvWriter.flush();
        csvWriter.close();

        System.out.println("Đổi password thành công");
    }

    private static void editDiemSinhVien(String mssv, String idClassSubject, float diemGK, float diemCK, float diemKhac, float diemTong) throws IOException {
        String pathFile = "./properties/files/Diem/" + idClassSubject + ".csv";
        List<Diem> lstDiem = new ArrayList<Diem>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("MSSV")) {
                Diem diemTmp = new Diem("", "", (float) 0/1, (float)0/1, (float)0/1, (float)0/1);

                diemTmp.setMSSV(data[0]);
                diemTmp.setName(data[1]);
                diemTmp.setDiemGK(Float.parseFloat(data[2]));
                diemTmp.setDiemCK(Float.parseFloat(data[3]));
                diemTmp.setDiemKhac(Float.parseFloat(data[4]));
                diemTmp.setDiemTong(Float.parseFloat(data[5]));

                lstDiem.add(diemTmp);
            }
        }
        csvReader.close();

        for(Diem diem : lstDiem){
            if(diem.getMSSV().equals(mssv)){
                diem.setDiemGK(diemGK);
                diem.setDiemCK(diemCK);
                diem.setDiemKhac(diemKhac);
                diem.setDiemTong(diemTong);
            }
        }

        writeFileSystemDiem(lstDiem, pathFile);
        System.out.println("Sửa điểm thành công");
    }

    private static SinhVien getStudentByMssv(String mssv, String idClass, String idSubject) throws IOException {
        String pathFile = "./properties/files/System.csv";
        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("MSSV")) {
                SinhVien svTmp = new SinhVien("", "", "", "", "");
                for (int i = 0; i < data.length; i++) {
                    if (i == 0)
                        svTmp.setMSSV(data[i]);
                    if (i == 1)
                        svTmp.setName(data[i]);
                    if (i == 2)
                        svTmp.setGender(data[i]);
                    if (i == 3)
                        svTmp.setIdCard(data[i]);
                    if(i == 4)
                        svTmp.setIdClass(data[i]);
                }
                if(svTmp.getMSSV().equals(mssv)){
                    csvReader.close();
                    return svTmp;
                }
            }
        }
        csvReader.close();
        return null;
    }

    private static void addStudentToSystem(String namePath, String idClass, String idSubject, String mssv) throws IOException {
        SinhVien sv = getStudentByMssv(mssv, idClass, idSubject);
        System.out.println(sv);
        if(sv != null){
            FileWriter csvWriter = new FileWriter(namePath, true);

            csvWriter.append(sv.getMSSV());
            csvWriter.append(",");
            csvWriter.append(sv.getName());
            csvWriter.append(",");
            csvWriter.append(sv.getGender());
            csvWriter.append(",");
            csvWriter.append(sv.getIdCard());
            csvWriter.append(",");
            csvWriter.append(sv.getIdClass());
            csvWriter.append("\n");


            csvWriter.close();
            System.out.println("Đăng ký môn thành công");
        }else{
            System.out.println("Sinh viên không tồn tại");
        }
    }

    private static void removeStudentFromSystem(String namePath, String idClass, String mssv) throws IOException {
        List<SinhVien> lstSv = readFileSinhVienByIdClass(namePath, idClass);
        for(SinhVien sv : lstSv){
            if(sv.getMSSV().equals(mssv)){
                lstSv.remove(sv);
                break;
            }
        }

        FileWriter csvWriter = new FileWriter(namePath, false);
        csvWriter.append("MSSV,Họ Tên,Giới Tính,CMND,Lớp\n");

        for(SinhVien sv : lstSv){
            csvWriter.append(sv.getMSSV());
            csvWriter.append(",");
            csvWriter.append(sv.getName());
            csvWriter.append(",");
            csvWriter.append(sv.getGender());
            csvWriter.append(",");
            csvWriter.append(sv.getIdCard());
            csvWriter.append(",");
            csvWriter.append(sv.getIdClass());
            csvWriter.append("\n");
        }

        csvWriter.close();
        System.out.println("Hủy đăng ký môn thành công");
    }

    private static List readFileSystem(String pathFile) throws IOException {
        List<SinhVien> lstSV = new ArrayList<SinhVien>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("MSSV")) {
                SinhVien svTmp = new SinhVien("", "", "", "", "");
                for (int i = 0; i < data.length; i++) {
                    if (i == 0)
                        svTmp.setMSSV(data[i]);
                    if (i == 1)
                        svTmp.setName(data[i]);
                    if (i == 2)
                        svTmp.setGender(data[i]);
                    if (i == 3)
                        svTmp.setIdCard(data[i]);
                    if (i == 4)
                        svTmp.setIdClass(data[i]);
                }
                lstSV.add(svTmp);
            }
        }
        csvReader.close();
        return lstSV;
    }

    private static List readFileSystemTKB(String pathFile) throws IOException {
        List<ThoiKhoaBieu> lstTKB = new ArrayList<ThoiKhoaBieu>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("Mã Môn")) {
                ThoiKhoaBieu tkbTmp = new ThoiKhoaBieu("", "", "", "");
                for (int i = 0; i < data.length; i++) {
                    if (i == 0)
                        tkbTmp.setIdSubject(data[i]);
                    if (i == 1)
                        tkbTmp.setNameSubject(data[i]);
                    if (i == 2)
                        tkbTmp.setClassRoom(data[i]);
                    if (i == 3)
                        tkbTmp.setIdClass(data[i]);
                }
                lstTKB.add(tkbTmp);
            }
        }
        csvReader.close();
        return lstTKB;
    }

    private static List readFileSystemTKBByIdClass(String pathFile, String idClass) throws IOException {
        List<ThoiKhoaBieu> lstTKB = new ArrayList<ThoiKhoaBieu>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("Mã Môn") && data[3].equals(idClass)) {
                ThoiKhoaBieu tkbTmp = new ThoiKhoaBieu("", "", "", "");
                for (int i = 0; i < data.length; i++) {
                    if (i == 0)
                        tkbTmp.setIdSubject(data[i]);
                    if (i == 1)
                        tkbTmp.setNameSubject(data[i]);
                    if (i == 2)
                        tkbTmp.setClassRoom(data[i]);
                    if (i == 3)
                        tkbTmp.setIdClass(data[i]);
                }
                lstTKB.add(tkbTmp);
            }
        }
        csvReader.close();
        return lstTKB;
    }

    private static List readFileSinhVien(String pathFile, String idClass) throws IOException {
        List<SinhVien> lstSV = new ArrayList<SinhVien>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("MSSV")) {
                SinhVien svTmp = new SinhVien("", "", "", "", "");
                for (int i = 0; i < data.length; i++) {
                    if (i == 0)
                        svTmp.setMSSV(data[i]);
                    if (i == 1)
                        svTmp.setName(data[i]);
                    if (i == 2)
                        svTmp.setGender(data[i]);
                    if (i == 3)
                        svTmp.setIdCard(data[i]);
                }
                svTmp.setIdClass(idClass);
                lstSV.add(svTmp);
            }
        }
        csvReader.close();
        return lstSV;
    }

    private static List readFileSinhVienTKB(String pathFile) throws IOException {
        List<SinhVien> lstSV = new ArrayList<SinhVien>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("MSSV")) {
                SinhVien svTmp = new SinhVien("", "", "", "", "");
                for (int i = 0; i < data.length; i++) {
                    if (i == 0)
                        svTmp.setMSSV(data[i]);
                    if (i == 1)
                        svTmp.setName(data[i]);
                    if (i == 2)
                        svTmp.setGender(data[i]);
                    if (i == 3)
                        svTmp.setIdCard(data[i]);
                    if (i == 4)
                        svTmp.setIdClass(data[i]);
                }
                lstSV.add(svTmp);
            }
        }
        csvReader.close();
        return lstSV;
    }

    private static List readFileSinhVienByIdClass(String pathFile, String idClass) throws IOException {
        List<SinhVien> lstSV = new ArrayList<SinhVien>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("MSSV") && data[4].equals(idClass)) {
                SinhVien svTmp = new SinhVien("", "", "", "", "");
                for (int i = 0; i < data.length; i++) {
                    if (i == 0)
                        svTmp.setMSSV(data[i]);
                    if (i == 1)
                        svTmp.setName(data[i]);
                    if (i == 2)
                        svTmp.setGender(data[i]);
                    if (i == 3)
                        svTmp.setIdCard(data[i]);
                }
                svTmp.setIdClass(idClass);
                lstSV.add(svTmp);
            }
        }
        csvReader.close();
        return lstSV;
    }

    private static List readFileTKB(String pathFile, String idClass) throws IOException {
        List<ThoiKhoaBieu> lstTKB = new ArrayList<ThoiKhoaBieu>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("STT")) {
                ThoiKhoaBieu tkbTmp = new ThoiKhoaBieu("", "", "", "");
                for (int i = 0; i < data.length; i++) {
                    if (i == 1)
                        tkbTmp.setIdSubject(data[i]);
                    if (i == 2)
                        tkbTmp.setNameSubject(data[i]);
                    if (i == 3)
                        tkbTmp.setClassRoom(data[i]);
                }
                tkbTmp.setIdClass(idClass);
                lstTKB.add(tkbTmp);
            }
        }
        csvReader.close();
        return lstTKB;
    }

    private static List readFileDiem(String pathFile, String idClass) throws IOException {
        List<Diem> lstDiem = new ArrayList<Diem>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("STT")) {
                Diem diemTmp = new Diem("", "", (float) 0/1, (float)0/1, (float)0/1, (float)0/1);

                diemTmp.setMSSV(data[1]);
                diemTmp.setName(data[2]);
                diemTmp.setDiemGK(Float.parseFloat(data[3]));
                diemTmp.setDiemCK(Float.parseFloat(data[4]));
                diemTmp.setDiemKhac(Float.parseFloat(data[5]));
                diemTmp.setDiemTong(Float.parseFloat(data[6]));

                lstDiem.add(diemTmp);
            }
        }
        csvReader.close();
        return lstDiem;
    }

    private static void analyticsDiem(String pathFile) throws IOException {
        List<Diem> lstDiem = new ArrayList<Diem>();

        BufferedReader csvReader = new BufferedReader(new FileReader(pathFile));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            if (!data[0].equals("MSSV")) {
                Diem diemTmp = new Diem("", "", (float) 0/1, (float)0/1, (float)0/1, (float)0/1);

                diemTmp.setMSSV(data[0]);
                diemTmp.setName(data[1]);
                diemTmp.setDiemGK(Float.parseFloat(data[2]));
                diemTmp.setDiemCK(Float.parseFloat(data[3]));
                diemTmp.setDiemKhac(Float.parseFloat(data[4]));
                diemTmp.setDiemTong(Float.parseFloat(data[5]));

                lstDiem.add(diemTmp);
            }
        }
        csvReader.close();

        List<Diem> lstDau = new ArrayList<>();
        List<Diem> lstRot = new ArrayList<>();
        for(Diem diem : lstDiem){
            if(diem.getDiemTong() >= 5){
                lstDau.add(diem);
            }else{
                lstRot.add(diem);
            }
        }

        System.out.println("Danh sách sinh viên đậu:");
        List<Object> lstObj = new ArrayList<Object>();
        lstObj.addAll(lstDau);
        printArray(lstObj);

        System.out.println("Danh sách sinh viên rớt:");
        List<Object> lstObj2 = new ArrayList<Object>();
        lstObj2.addAll(lstRot);
        printArray(lstObj2);

        System.out.println("Phần trăm / số lượng đậu: " + ((float)lstDau.size() / lstDiem.size()) * 100 + "%, (" + lstDau.size() + " sinh viên)");
        System.out.println("Phần trăm / số lượng rớt: " + ((float)lstRot.size() / lstDiem.size()) * 100 + "%, (" + lstRot.size() + " sinh viên)");
    }

    private static void writeFileSystem(List<SinhVien> lstSV, String pathFileSystem) throws IOException {
        FileWriter csvWriter = new FileWriter(pathFileSystem, true);
        List lstSystem = readFileSystem(pathFileSystem);

        for (SinhVien sinhVien : lstSV) {
            SinhVien svTmp = (SinhVien) sinhVien;
            if (!isExistsInList(lstSystem, svTmp)) {
                csvWriter.append(svTmp.getMSSV());
                csvWriter.append(",");
                csvWriter.append(svTmp.getName());
                csvWriter.append(",");
                csvWriter.append(svTmp.getGender());
                csvWriter.append(",");
                csvWriter.append(svTmp.getIdCard());
                csvWriter.append(",");
                csvWriter.append(svTmp.getIdClass());
                csvWriter.append("\n");
            }else{
                System.out.println("Sinh viên: " + svTmp.getMSSV() + " đã tồn tại trong hệ thống.");
            }
        }

        csvWriter.flush();
        csvWriter.close();
    }

    private static void writeFileSystemTKB(List<ThoiKhoaBieu> lstTKB, String pathFileSystem) throws IOException {
        FileWriter csvWriter = new FileWriter(pathFileSystem, true);
        List lstSystem = readFileSystemTKB(pathFileSystem);

        for (ThoiKhoaBieu tkb : lstTKB) {
            ThoiKhoaBieu tkbTmp = (ThoiKhoaBieu) tkb;
            if (!isExistsInListTKB(lstSystem, tkbTmp)) {
                csvWriter.append(tkbTmp.getIdSubject());
                csvWriter.append(",");
                csvWriter.append(tkbTmp.getNameSubject());
                csvWriter.append(",");
                csvWriter.append(tkbTmp.getClassRoom());
                csvWriter.append(",");
                csvWriter.append(tkbTmp.getIdClass());
                csvWriter.append("\n");

                // Clone and insert file
                String namePath = "./properties/files/" + tkbTmp.getIdClass() + "-" + tkbTmp.getIdSubject() + ".csv";
                writeFileSystemClassSubject(namePath, tkbTmp);
            }else{
                System.out.println("Môn học " + tkbTmp.getNameSubject() + " và lớp " + tkbTmp.getIdClass() + " đã tồn tại trong hệ thống.");
            }
        }

        csvWriter.flush();
        csvWriter.close();
    }

    private static void writeFileSystemDiem(List<Diem> lstDiem, String pathFileSystem) throws IOException {
        FileWriter csvWriter = new FileWriter(pathFileSystem, false);

        csvWriter.append("MSSV,Họ Tên,Điểm GK,Điểm CK,Điểm khác,Điểm Tổng\n");

        for (Diem diem : lstDiem) {
            csvWriter.append(diem.getMSSV());
            csvWriter.append(",");
            csvWriter.append(diem.getName());
            csvWriter.append(",");
            csvWriter.append(diem.getDiemGK().toString());
            csvWriter.append(",");
            csvWriter.append(diem.getDiemCK().toString());
            csvWriter.append(",");
            csvWriter.append(diem.getDiemKhac().toString());
            csvWriter.append(",");
            csvWriter.append(diem.getDiemTong().toString());
            csvWriter.append("\n");
        }

        csvWriter.flush();
        csvWriter.close();
    }

    private static void writeFileSystemClassSubject(String namePath, ThoiKhoaBieu tkbTmp) throws IOException {
        FileWriter csvWriter = new FileWriter(namePath, false);
        List<SinhVien> lstSV = readFileSinhVienByIdClass("./properties/files/System.csv", tkbTmp.getIdClass());

        csvWriter.append("MSSV,Họ Tên,Giới Tính,CMND,Lớp\n");

        for(SinhVien sv : lstSV){
          csvWriter.append(sv.getMSSV());
          csvWriter.append(",");
          csvWriter.append(sv.getName());
          csvWriter.append(",");
          csvWriter.append(sv.getGender());
          csvWriter.append(",");
          csvWriter.append(sv.getIdCard());
          csvWriter.append(",");
          csvWriter.append(sv.getIdClass());
          csvWriter.append("\n");
        }

        csvWriter.close();
    }

    // Function Static
    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException e) {
            System.out.println("*** ERROR: Vui lòng nhập số.\n");
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    private static List<String> listFilesForFolder(String pathFolder) {
        File folder = new File(pathFolder);
        File[] listOfFiles = folder.listFiles();

        List<String> lstFile = new ArrayList<String>();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                lstFile.add(file.getName());
            }
        }
        return lstFile;
    }

    private static boolean isExistsInList(List<SinhVien> lstSV, SinhVien sv) {
        for (SinhVien svLoop : lstSV) {
            if (sv.getMSSV().equals(svLoop.getMSSV())) {
                return true;
            }
        }
        return false;
    }

    private static boolean isExistsInListTKB(List<ThoiKhoaBieu> lstTKB, ThoiKhoaBieu tkb) {
        for (ThoiKhoaBieu tkbLoop : lstTKB) {
            if (tkb.getIdSubject().equals(tkbLoop.getIdSubject()) && tkb.getIdClass().equals(tkbLoop.getIdClass())) {
                return true;
            }
        }
        return false;
    }
    // End function static

    private static <E> void printArray(List<Object> elements) {
        for (Object element : elements) {
            System.out.println(element.toString());
        }
    }
}
